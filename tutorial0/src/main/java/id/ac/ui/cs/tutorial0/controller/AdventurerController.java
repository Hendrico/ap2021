package id.ac.ui.cs.tutorial0.controller;

import id.ac.ui.cs.tutorial0.service.AdventurerCalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdventurerController {

    @Autowired
    private AdventurerCalculatorService adventurerCalculatorService;

    @RequestMapping("/adventurer/countPower")
    private String showAdventurerPowerFromBirthYear(@RequestParam("birthYear")int birthYear, Model model) {
        int power = adventurerCalculatorService.countPowerPotensialFromBirthYear(birthYear);
        model.addAttribute("power", power);
        String rank = "";
        if (power < 0){
            rank = "ERROR";
        }
        else if (power <= 20000){
            rank = "C";
        } else if (power <= 100000) {
            rank = "B";
        } else if (power > 100000) {
            rank = "A";
        }
        model.addAttribute("rank", rank);
        return "calculator";
    }
}
